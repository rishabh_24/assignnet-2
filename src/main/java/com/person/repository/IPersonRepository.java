package com.person.repository;

import java.util.List;

import com.person.exceptions.PersonNotFoundException;
import com.person.model.Person;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface IPersonRepository extends MongoRepository<Person, Integer> {
    List<Person> findAll();

    @Query("{name : ?0}")
    List<Person> findByName(String name) throws PersonNotFoundException;

    @Query("{city : ?0}")
    List<Person> findByCity(String city) throws PersonNotFoundException;

    @Query("{state : ?0}")
    List<Person> findByState(String state) throws PersonNotFoundException;

    @Query("{country : ?0}")
    List<Person> findByCountry(String country) throws PersonNotFoundException;

    @Query("{zipcode: ?0}")
    List<Person> findByZipcode(String zipcode) throws PersonNotFoundException;

    @Query("{age: ?0}")
    List<Person> findByAge(int age) throws PersonNotFoundException;

    @Query("{$and : [{name : ?0}, {city : ?1 }]}")
    List<Person> findByNameAndCity(String name, String city) throws PersonNotFoundException;

    @Query("{$and : [{state : ?0}, {country : ?1 }]}")
    List<Person> findByStateAndCountry(String state, String country) throws PersonNotFoundException;

    @Query("{$and : [{city : ?0}, {zipcode : ?1 }]}")
    List<Person> findByCityAndZipcode(String city, String zipcode) throws PersonNotFoundException;
}
