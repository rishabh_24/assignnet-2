package com.person.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document

public class Person {
    @Id
    private Integer personId;
    private String name;
    private String city;
    private String state;
    private String country;
    private String zipcode;
    private int age;

    public Person(Integer personId, String name, String city, String state, String country, String zipcode, int age) {
        this.personId = personId;
        this.name = name;
        this.city = city;
        this.state = state;
        this.country = country;
        this.zipcode = zipcode;
        this.age = age;
    }

    public String getZipcode() {
        return zipcode;
    }

    public Integer getPersonId() {
        return personId;
    }

    @Override
    public String toString() {
        return "Person [personId="
                + personId + ", age=" + age + ", city=" + city + ", country=" + country + ", name=" + name + " , state="
                + state + ", zipcode=" + zipcode + "]";
    }

}
