package com.person.service;

import java.util.List;

import com.person.exceptions.PersonNotFoundException;
import com.person.model.Person;

public interface IPersonService {
    void addListOfPersons(List<Person> persons);

    void updatePerson(Person person);

    void deletePerson(int id);

    List<Person> getAllPerson();

    List<Person> getPersonByName(String name) throws PersonNotFoundException;

    List<Person> getPersonByCity(String city) throws PersonNotFoundException;

    List<Person> getPersonByState(String state) throws PersonNotFoundException;

    List<Person> getPersonByCountry(String country) throws PersonNotFoundException;

    List<Person> getPersonByZipCode(String zipcode) throws PersonNotFoundException;

    List<Person> getPersonByAge(int age) throws PersonNotFoundException;

    Person getPersonById(Integer id) throws PersonNotFoundException;

    List<Person> getPersonByNameAndCity(String name, String city) throws PersonNotFoundException;

    List<Person> getPersonByStateAndCountry(String state, String country) throws PersonNotFoundException;

    List<Person> getPersonByCityAndZipcode(String city, String zipcode) throws PersonNotFoundException;
}
