package com.person.service;

import java.util.List;

import com.person.exceptions.PersonNotFoundException;
import com.person.model.Person;
import com.person.repository.IPersonRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PesronServiceImpl implements IPersonService {

    private IPersonRepository personRepository;

    @Autowired
    public void setPersonRepository(IPersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public List<Person> getPersonByName(String name) {
        List<Person> personsByName = null;
        try {
            personsByName = personRepository.findByName(name);
        } catch (PersonNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return personsByName;
    }

    @Override
    public List<Person> getPersonByCity(String city) {
        List<Person> personsByCity = null;
        try {
            personsByCity = personRepository.findByCity(city);
        } catch (PersonNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return personsByCity;
    }

    @Override
    public List<Person> getPersonByState(String state) {
        List<Person> personsByState = null;
        try {
            personsByState = personRepository.findByState(state);
        } catch (PersonNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return personsByState;
    }

    @Override
    public List<Person> getPersonByCountry(String country) {
        List<Person> personsByCountry = null;
        try {
            personsByCountry = personRepository.findByCountry(country);
        } catch (PersonNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return personsByCountry;
    }

    @Override
    public List<Person> getPersonByZipCode(String zipcode) {
        List<Person> personsByZipcode = null;
        try {
            personsByZipcode = personRepository.findByZipcode(zipcode);
        } catch (PersonNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return personsByZipcode;
    }

    @Override
    public List<Person> getPersonByAge(int age) {
        List<Person> personsByAge = null;
        try {
            personsByAge = personRepository.findByAge(age);
        } catch (PersonNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return personsByAge;
    }

    @Override
    public Person getPersonById(Integer id) throws PersonNotFoundException {
        return personRepository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException("No person with such id exist!!"));
    }

    @Override
    public List<Person> getPersonByNameAndCity(String name, String city) {
        List<Person> personByNameAndCity = null;
        try {
            personByNameAndCity = personRepository.findByNameAndCity(name, city);
        } catch (PersonNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return personByNameAndCity;
    }

    @Override
    public List<Person> getPersonByStateAndCountry(String state, String country) {
        List<Person> personByStateAndCountry = null;
        try {
            personByStateAndCountry = personRepository.findByStateAndCountry(state, country);
        } catch (PersonNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return personByStateAndCountry;
    }

    @Override
    public List<Person> getPersonByCityAndZipcode(String city, String zipcode) {
        List<Person> personByCityAndZipcode = null;
        try {
            personByCityAndZipcode = personRepository.findByCityAndZipcode(city, zipcode);
        } catch (PersonNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return personByCityAndZipcode;
    }

    @Override
    public void addListOfPersons(List<Person> persons) {
        personRepository.insert(persons);
        System.out.println("List Of persons has been added successfully!");
    }

    @Override
    public void updatePerson(Person person) {
        System.out.println("Before : " + personRepository.findById(person.getPersonId()));
        personRepository.save(person);
        System.out.println("After : " + personRepository.findById(person.getPersonId()));
    }

    @Override
    public void deletePerson(int id) {
        personRepository.deleteById(id);
        System.out.println("Person with id " + id + " is deleted successfully!");
    }

    @Override
    public List<Person> getAllPerson() {
        return personRepository.findAll();
    }

}
