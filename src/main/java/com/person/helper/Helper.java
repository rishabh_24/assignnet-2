package com.person.helper;

import com.person.model.Person;
import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Helper {

    private static List<String> names = new ArrayList<String>();
    private static List<String> cities = new ArrayList<String>();
    private static List<String> states = new ArrayList<String>();
    private static List<String> countries = new ArrayList<String>();
    private static List<String> zipcodes = new ArrayList<String>();
    static Lorem lorem = LoremIpsum.getInstance();

    public String getName() {
        return names.get(customRandom(10));
    }

    public String getCity() {
        return cities.get(customRandom(10));
    }

    public String getState() {
        return states.get(customRandom(10));
    }

    public String getCountry() {
        return countries.get(customRandom(10));
    }

    public String getZipcode() {
        return zipcodes.get(customRandom(10));
    }

    private void chunkGenerator() {
        for (int i = 0; i < 10; i++) {
            names.add(lorem.getName());
            cities.add(lorem.getCity());
            states.add(lorem.getStateFull());
            countries.add(lorem.getCountry());
            zipcodes.add(lorem.getZipCode());
        }
    }

    private int customRandom(int maximum) {
        return (int) (maximum * Math.random());
    }

    public Person documentGenerator(int id) {
        String name = names.get(customRandom(10));
        String city = cities.get(customRandom(10));
        String state = states.get(customRandom(10));
        String country = countries.get(customRandom(10));
        String zipcode = zipcodes.get(customRandom(10));
        int age = customRandom(100);
        return new Person(id, name, city, state, country, zipcode, age);
    }

    public List<Person> listOfDocumentGenerator(Integer maxRcords) {
        chunkGenerator();
        List<Person> personsList = new ArrayList<Person>();
        for (int id = 1; id <= maxRcords; id++) {
            personsList.add(documentGenerator(id));
        }
        return personsList;
    }
}
