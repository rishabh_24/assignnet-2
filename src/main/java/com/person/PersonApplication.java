package com.person;

import com.person.helper.Helper;
import com.person.model.Person;
import com.person.service.IPersonService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class PersonApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(PersonApplication.class, args);
	}

	@Autowired
	private IPersonService iPersonService;

	@Autowired
	Helper help;

	@Override
	public void run(String... args) throws Exception {
		List<Person> persons = help.listOfDocumentGenerator((int) (1e3));
		System.out.println("Generating Data\n");
		iPersonService.addListOfPersons(persons);

		System.out.println("All persons\n");
		iPersonService.getAllPerson().stream().forEach(System.out::println);

		System.out.println("\nUpdating person");
		Person personToBeUpadted = help.documentGenerator((int) (1e3 *
				Math.random()));
		System.out.println(personToBeUpadted);
		iPersonService.updatePerson(personToBeUpadted);

		System.out.println("\nDeleting person\n");
		int idOfPersonToBeDeleted = (int) (1e3 * Math.random());
		iPersonService.deletePerson(idOfPersonToBeDeleted);

		System.out.println("\nBy name\n");
		iPersonService.getPersonByName(help.getName()).stream()
				.forEach(System.out::println);

		System.out.println("\nBy city \n");
		iPersonService.getPersonByCity(help.getCity()).stream()
				.forEach(System.out::println);

		System.out.println("\n Buy State\n");
		iPersonService.getPersonByState(help.getState()).stream()
				.forEach(System.out::println);

		System.out.println("\n Buy Country\n");
		iPersonService.getPersonByCountry(help.getCountry()).stream()
				.forEach(System.out::println);

		System.out.println("\n By Zipcode \n");
		iPersonService.getPersonByZipCode(help.getZipcode()).stream()
				.forEach(System.out::println);

		System.out.println("\n By Age \n");
		iPersonService.getPersonByAge((int) (70 *
				Math.random())).stream().forEach(System.out::println);

		System.out.println("\n By Name and City \n");
		iPersonService.getPersonByNameAndCity(help.getName(),
				help.getCity()).stream().forEach(System.out::println);

		System.out.println("\n By State and Country \n");
		iPersonService.getPersonByStateAndCountry(help.getState(),
				help.getCountry()).stream()
				.forEach(System.out::println);

		System.out.println("\n By City and Zipcode \n");
		iPersonService.getPersonByCityAndZipcode(help.getCity(),
				help.getZipcode()).stream()
				.forEach(System.out::println);
	}

}
